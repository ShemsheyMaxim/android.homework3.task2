package com.maxim.task1;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button buttonStart;
    private View background;
    private int color;
    private int[] backgroundApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        backgroundApplication = getResources().getIntArray(R.array.backgroundApplication);

        background = (View) findViewById(R.id.layout_id);

        buttonStart = (Button) findViewById(R.id.button_start);
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setVisibility(View.GONE);
                new ChangeBackgroundAsyncTask().execute();
            }
        });
    }
    private class ChangeBackgroundAsyncTask extends AsyncTask<Void,Integer,Void> {

        @Override
        protected Void doInBackground(Void... params) {
            while (true) {
                for (int i = 0; i < backgroundApplication.length - 1; i++) {
                    color = backgroundApplication[i];
                    publishProgress(color);
                    SystemClock.sleep(3000);
                }
                for (int i = 3; i >= backgroundApplication.length - 3; i--) {
                    color = backgroundApplication[i];
                    publishProgress(color);
                    SystemClock.sleep(3000);
                }

            }
        }
        @Override
        protected void onProgressUpdate(Integer... values){
            super.onProgressUpdate(values);
            if (background != null){
                background.setBackgroundColor(color);
            }
        }
    }
}

